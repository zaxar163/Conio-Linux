#include "coniol.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv) {
    cout << "Symbol... ";
    char a = getch();
    cout << endl;
    cout << "Symbol: ";
    cout << a << endl;
    cout << "Symbol... ";
    char b = getche();
    cout << endl;
    cout << "Symbol: ";
    cout << b << endl;
    cout << "putch() test: echo U..." << endl;
    int sym = 85;
    char charc = static_cast<char>(sym);
    putch(charc);
    cout << endl << "Done test!" << endl;
    cout << "Press any key to clrscr... " << endl;    
    getch();
    clrscr();
    cout << "Works!" << endl;
    cout << "Press any key to exit... " << endl;   
    getch();
    return 0;
}



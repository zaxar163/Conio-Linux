#include "coniol.h"
#ifdef __linux

#ifdef __cplusplus
extern "C" {
#endif
static struct termios __oldset, __newset;

/* Initialize new terminal i/o settings */
static void __initTermios(int echo) 
{
  tcgetattr(0, &__oldset); /* grab old terminal i/o settings */
  __newset = __oldset; /* make new settings same as old settings */
  __newset.c_lflag &= ~ICANON; /* disable buffered i/o */
  __newset.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &__newset); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
static void __resetTermios(void) 
{
  tcsetattr(0, TCSANOW, &__oldset);
}

/* Read 1 character - echo defines echo mode */
static char __getch_(int echo) 
{
  char ch;
  __initTermios(echo);
  ch = getchar();
  __resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void) 
{
  return __getch_(0);
}

/* Read 1 character with echo */
char getche(void) 
{
  return __getch_(1);
}

void clrscr(void) 
{  
    printf("\033[2J"); /* Clear the entire screen. */ 
    printf("\033[0;0f"); /* Move cursor to the top left hand corner */ 
}
#ifdef __cplusplus
}
void putch(char ch) {
    std::cout << ch;
}
#endif
#endif

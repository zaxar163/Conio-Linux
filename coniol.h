#ifndef CONIOL_H
#define CONIOL_H 1

#include <stdio.h>

#ifndef __linux
    #include <conio.h>
#else
    #include <unistd.h>
    #include <termios.h>
    #ifdef __cplusplus
        #include <iostream>
        extern "C" {
        #define __SET_CAST(type,value) static_cast<type>(value)
    #else
        #define __SET_CAST(type,value) (type)(value)
    #endif

    char getch(void);
    char getche(void);
    void clrscr(void);

    #ifdef __cplusplus
    }
    void putch(char ch);

    #endif
#endif

#endif

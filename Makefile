CFLAGS += -fPIC

all: libconiol.a libconiol.so coniotest coniotest_noputch

coniol.o: coniol.c coniol.h
	$(CXX) $(CFLAGS) -c coniol.c -o coniol.o
	cp coniol.h ./include/

libconiol.so: coniol.o
	$(CXX) $(CFLAGS) -shared coniol.o -o libconiol.so
	cp libconiol.so ./lib/

libconiol.a: coniol.o
	ar cr libconiol.a coniol.o
	mkdir -p ./include/ ./lib/ ./bin/
	cp libconiol.a ./lib/

clean:
	rm -f *.o *.a *.so coniotest.shared coniotest coniotest_noputch.shared coniotest_noputch
	rm -rf ./include/ ./lib/ ./bin/

coniotest: libconiol.a libconiol.so coniotest.cpp
	$(CXX) $(CFLAGS) coniotest.cpp -L. -lconiol --static -o coniotest
	$(CXX) $(CFLAGS) coniotest.cpp -L. -lconiol -o coniotest.shared
	cp coniotest.shared ./bin/
	cp coniotest ./bin/

coniotest_noputch: libconiol.a libconiol.so coniotest_noputch.cpp
	$(CXX) $(CFLAGS) coniotest_noputch.cpp -L. -lconiol --static -o coniotest_noputch
	$(CXX) $(CFLAGS) coniotest_noputch.cpp -L. -lconiol -o coniotest_noputch.shared
	cp coniotest_noputch.shared ./bin/
	cp coniotest_noputch ./bin/

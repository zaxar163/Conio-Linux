#include "coniol.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv) {
    cout << "Symbol... ";
    char a = getch();
    cout << endl;
    cout << "Symbol: ";
    cout << a << endl;
    cout << "Symbol... ";
    char b = getche();
    cout << endl;
    cout << "Symbol: ";
    cout << b << endl;
    cout << "Press any key to clrscr... " << endl;    
    getch();
    clrscr();
    cout << "Works!" << endl;
    cout << "Press any key to exit... " << endl;   
    getch();
    return 0;
}
